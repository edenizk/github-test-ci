﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Animator animator;
    public AMonster monster;

    public Transform agro;
    //public string basePoint;
    public Transform attackPose;

    public Vector3 basePosition;
    //public GameObject prefab;

    public LayerMask whatIsEnimies;

    int currentHealth;
    public HealthBar healthBar;

    public Rigidbody2D rb;
    bool isChasing;
    bool isAttacking;
    Vector2 movement;

    public ParticleSystem particularHitted;
    
    float nextAttackTime = 0f;

    private float dazedTime;
    public float healthBarTime;
    private float speed;
    void Start()
    {
        GetComponent<Renderer>().enabled = true;
        animator.ResetTrigger("Attack");
        animator.SetFloat("AttackSpeed", monster.attackSpeed);
        currentHealth = monster.health;
        healthBar.SetMaxHealth(monster.health);
        healthBar.hide();
        isChasing = false;
        isAttacking = false;
        dazedTime = 0;
        healthBarTime = 0;
        particularHitted.Stop();
        speed = monster.speed;
        basePosition = transform.position;
        particularHitted.startSize = 1;
        this.enabled = true;
        GetComponent<Collider2D>().enabled = true;
        animator.SetBool("isDead", false);
    }

    private void Update()
    {
        if(dazedTime <= 0)
        {
            speed = monster.speed;
        }
        else
        {
            speed = 0;
            dazedTime -= Time.deltaTime;
        }

        if(healthBarTime <= 0)
        {
            healthBar.hide();
        }
        else
        {
            healthBarTime -= Time.deltaTime;
        }

        if (!animator.GetBool("isDead"))
        {
            Chase();
            Attack();

            animator.SetFloat("Horizontal", movement.x);
            //more optimized way to sqr magnitude for better performence
            animator.SetFloat("Speed", movement.sqrMagnitude);
            if (movement.sqrMagnitude > 0.1)
            {
                //For determine direction of face on stop
                animator.SetFloat("LastHorizontal", movement.x);
            }
        }
      
    }

    void FixedUpdate()
    {
        //Movement
        //rb.position for the current position and movement for the direction
        //fixed delta time is for the constant movement
        if (!isAttacking && !animator.GetBool("isDead"))
        {
            if (isChasing)
                rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
            else
            {
                //if not chasing comeback to base if not have base stop
                if (
                    basePosition != null &&
                    !(basePosition.x - transform.position.x < 1)
                    || !(basePosition.y - transform.position.y < 1))
                {
                    Vector3 direction = basePosition - transform.position;
                    direction.Normalize();
                    movement = direction;
                    rb.MovePosition(rb.position + movement * speed * Time.fixedDeltaTime);
                }
                else
                {
                    movement.x = 0;
                    movement.y = 0;
                    animator.SetFloat("LastHorizontal", 0);

                }
            }
        }else if (isAttacking)
        {
            movement.x = 0;
            movement.y = 0;
        }
         
    }

    public void TakeDamage(int damage)
    {
        dazedTime = 1;
        healthBarTime = 5;
        particularHitted.Play();
        currentHealth -= damage;
        healthBar.show();
        healthBar.SetHealt(currentHealth);
        nextAttackTime = Time.time + 1 / monster.attackSpeed;

        animator.SetTrigger("Hurt");
        rb.velocity *= -1;
        if (currentHealth <= 0)
        {
            particularHitted.startSize = 4;
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Died");
        animator.SetBool("isDead", true);
        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
        Invoke("RemoveCharacter", 5f);
    }
    void RemoveCharacter()
    {
        //Destroy(gameObject);
        GetComponent<Renderer>().enabled = false;
        Reborn();
    }

    void Reborn()
    {
        transform.position = basePosition;
        Start();
    }

    public void StopParticular()
    {
        particularHitted.Stop();
    }

    void Attack()
    {
        if(Time.time >= nextAttackTime)
        {
            Collider2D[] hitEnimies = Physics2D.OverlapCircleAll(attackPose.position, monster.attackRange, whatIsEnimies);
            if (hitEnimies.Length != 0)
            {
                isAttacking = true;
                isChasing = false;
                animator.SetTrigger("Attack");
                nextAttackTime = Time.time + 1 / monster.attackSpeed;
            }
            else
            {
                isAttacking = false;
            }
        }
    }

    void Hit()
    {
        Collider2D[] hitEnimies = Physics2D.OverlapCircleAll(attackPose.position, monster.attackRange, whatIsEnimies);
        if (hitEnimies.Length != 0)
        {
            animator.SetTrigger("Attack");
            foreach (Collider2D enemy in hitEnimies)
            {
                Debug.Log("Monster hitted");
                enemy.GetComponent<PlayerCharacter>().TakeDamage(monster.damage);
            }
        }
    }


    void Chase()
    {
        if (!isAttacking)
        {
            Collider2D[] enemies = Physics2D.OverlapCircleAll(agro.position, monster.agroRange, whatIsEnimies);
            if (enemies.Length != 0)
            {
                Vector3 direction = enemies[0].transform.position - transform.position;
                direction.Normalize();
                movement = direction;
                isChasing = true;
            }
            else
            {
                isChasing = false;
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        if (agro == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(agro.position, monster.agroRange);
        Gizmos.color = Color.red;

         if (attackPose == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPose.position, monster.attackRange);
        Gizmos.color = Color.red;
    }
}
