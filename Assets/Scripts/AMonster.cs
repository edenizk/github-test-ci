﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AMonster : MonoBehaviour
{
    public abstract string name { get; set; }
    public abstract int health { get; set; }
    public abstract int damage { get; set; }
    public abstract float attackRange { get; set; }
    public abstract float attackSpeed { get; set; }

    public abstract float speed { get; set; }
    public abstract float agroRange { get; set; }

}
