﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomController : MonoBehaviour {

    public string currentWorldName = "StartScene";
    RoomInfo currentRoomInfo;
    public static RoomController instance;

    Queue<RoomInfo> loadRoomQueue = new Queue<RoomInfo> ();
    public List<Room> loadedRooms = new List<Room> ();

    bool isLoadingRoom = false;

    private void Awake () {
        instance = this;
    }

    void Start()
    {
        LoadRoom("room", 0, 0);
        Debug.Log("first room created");
        LoadRoom("room", 1, 0);
        Debug.Log("sec room created");
        LoadRoom("room", -1, 0);
        Debug.Log("third room created");
        LoadRoom("room", 0, 1);
        Debug.Log("fourth room created");
        LoadRoom("room", 0, -1);
        Debug.Log("test");
    }

    void Update()
    {
        UpdateRoomQueue();

    }

    void UpdateRoomQueue()
    {
        if (isLoadingRoom == true)
        {
            return;
        }

        if(loadRoomQueue.Count == 0)
        {
            return;
        }
        Debug.Log("test" + isLoadingRoom.ToString());


        currentRoomInfo = loadRoomQueue.Dequeue();
        isLoadingRoom = true;

        StartCoroutine(LoadRoomRoutine(currentRoomInfo));

    }

    public void LoadRoom( string name, int x, int y)
    {
        if (DoesRoomExist(x, y) == true)
        {
            return;
        }
        RoomInfo newRoomData = new RoomInfo();
        newRoomData.roomName = name;
        newRoomData.positionX = x;
        newRoomData.positionY = y;

        loadRoomQueue.Enqueue(newRoomData);
    }

    IEnumerator LoadRoomRoutine(RoomInfo info)
    {
        string roomName = info.roomName;

        AsyncOperation loadRoom = SceneManager.LoadSceneAsync(roomName, LoadSceneMode.Additive);

        while(loadRoom.isDone == false)
        {
            yield return null;
        }
    }

    public void RegisterRoom(Room room)
    {
        room.transform.position = new Vector3(
            currentRoomInfo.positionX * room.Width,
            currentRoomInfo.positionY * room.Height,
            0
        );

        room.PositionX = currentRoomInfo.positionX;
        room.PositionY = currentRoomInfo.positionY;
        room.name = currentWorldName + "-" + currentRoomInfo.roomName + " " + room.PositionX + ", " + room.PositionY;
        room.transform.parent = transform;

        isLoadingRoom = false;

        loadedRooms.Add(room);
    }

    public bool DoesRoomExist (int x, int y) {
        return loadedRooms.Find (item => item.PositionX == x && item.PositionY == y) != null;
    }
}