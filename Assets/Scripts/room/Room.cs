using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Room : MonoBehaviour {
    public int Width;
    public int Height;
    public int PositionX;
    public int PositionY;

    private void Start() {
        if(RoomController.instance == null){
            Debug.Log("wrong scene");
            return;
        }

        RoomController.instance.RegisterRoom(this);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(Width, Height, 0));
    }

    public Vector3 GetRoomCentre()
    {
        return new Vector3(PositionX * Width, PositionY * Height);
    }

}