﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public float startTimeBtwAttack;
    public Player player;

    public Animator animator;

    public Transform attackPose;
    public LayerMask whatIsEnimies;
    float nextAttackTime = 0f;

    public float offset;
    public GameObject arrow;
    public GameObject target;
    public Transform shotPoint;
    public bool isRangeAttack = true;

    private void Start()
    {
        animator.SetFloat("AttackSpeed", player.attackSpeed/2);
    }

    void Update()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - target.transform.position;
        float rotZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        target.transform.rotation = Quaternion.Euler(0f, 0f, rotZ + offset);
        //Debug.Log("difference" + difference);
        if (!player.isDead)
        {
            if (Time.time >= nextAttackTime)
            {
                if (Input.GetKey(KeyCode.Space) || Input.GetButtonDown("Fire1"))
                {
                    Attack();
                }
                if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire2"))
                {
                    RangeAttack();
                }
            }
        }
    }
    void Attack()
    {
        animator.SetTrigger("Attack");

        Collider2D[] hitEnimies = Physics2D.OverlapCircleAll(attackPose.position, player.attackRange, whatIsEnimies);
       // Debug.Log("attacking");

        foreach (Collider2D enemy in hitEnimies)
        {
            enemy.GetComponent<Enemy>().TakeDamage(player.damage);
            
        }
        nextAttackTime = Time.time + 1 / player.attackSpeed;
    }

    void RangeAttack()
    {
        animator.SetTrigger("Attack");
     
        Instantiate(arrow, shotPoint.position, shotPoint.rotation);

        nextAttackTime = Time.time + 1 / player.attackSpeed;
    }

    void Die()
    {
        Debug.Log("Died");
        animator.SetBool("isDead", true);
        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
        Invoke("RemoveCharacter", 5f);
        //GameObject go = Instantiate(PlayerPrefab);
        //playerController = go.GetComponent<PlayerController>();
    }

    void OnDrawGizmosSelected()
    {
        if(attackPose == null)
        {
            return;
        }
        Gizmos.DrawWireSphere(attackPose.position, player.attackRange);
        Gizmos.color = Color.red;
    }
}
