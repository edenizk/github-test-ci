﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class m1_enemy : AMonster

{
    /*   public new string name = "Monster 1";
       public new int health = 100;
       public new int attack = 100;
       public new float attackRange = 1.5f;
       public new float speed = 15f;
       */

    public m1_enemy()
    {
      name = "Monster 1";
      health = 100;
      damage = 20;
      attackRange = 1.8f;
      speed = 8f;
      agroRange = 15;
      attackSpeed = 1f;
    }
    public override string name { get; set; } 
    public override int health { get; set; } 
    public override int damage { get; set; } 
    public override float attackRange { get; set; } 
    public override float speed { get; set; }
    public override float agroRange { get; set; }

    public override float attackSpeed { get; set; }


}
