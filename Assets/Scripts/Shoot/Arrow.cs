﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float speed;
    public float lifeTime;
    public int damage = 10;

    public GameObject destroyEffect;
    public float particleLifeTime = 0.5f;

    void Start()
    {
        Invoke("DestroyArrow", lifeTime);
    }

    void Update()
    {
        transform.Translate(Vector2.right * speed * Time.deltaTime);
    }

    void DestroyArrow()
    {
        Vector3 effectPosition = new Vector3(transform.position.x, transform.position.y, -10);
        destroyEffect = Instantiate(destroyEffect, effectPosition, Quaternion.identity);
        Destroy(destroyEffect, particleLifeTime + 0.1f);
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        int name = collision.gameObject.layer;
        if(name == LayerMask.NameToLayer("Enemies"))
        {
            collision.collider.GetComponent<Enemy>().TakeDamage(damage);
            DestroyArrow();
        }else if(name != LayerMask.NameToLayer("Player"))
        {
            Debug.Log("Hitted sth: " + collision.gameObject.layer);
            DestroyArrow();
        }
    }
}
