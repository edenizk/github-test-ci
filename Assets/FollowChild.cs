﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowChild : MonoBehaviour
{
    public Transform child;

    void Update()
    {
        transform.position = child.position;
    }
}
